# anomaly-python

前置: RIC PLATFORM

docker build
```
cd anomaly-python
sudo docker build -it xApp-registry.local:5008/anomaly-python:1.0.0 .
```
#### onboard ####
```
export KONG_PROXY=`sudo kubectl get svc -n ricplt -l app.kubernetes.io/name=kong -o jsonpath='{.items[0].spec.clusterIP}'`
export APPMGR_HTTP=`sudo kubectl get svc -n ricplt --field-selector metadata.name=service-ricplt-appmgr-http -o jsonpath='{.items[0].spec.clusterIP}'`
export ONBOARDER_HTTP=`sudo kubectl get svc -n ricplt --field-selector metadata.name=service-ricplt-xapp-onboarder-http -o jsonpath='{.items[0].spec.clusterIP}'`
export CHART_REPO_URL=http://$ONBOARDER_HTTP:8080

cd init
dms_cli onboard -config-file.json embedded-schema.json

curl -L -X POST "http://$KONG_PROXY:32080/appmgr/ric/v1/xapps" --header 'Content-Type: application/json' --data-raw '{"xappName": "anomaly-python"}'
```


#### view log ####

```
sudo kubectl exec -it -n ricxapp `sudo kubectl get pod -n ricxapp -l app=ricxapp-anomaly-python -o jsonpath='{.items[0].metadata.name}'` -- tail -F /opt/anomalyXapp.log
```
#### undeploy ####
```
curl -H "Content-Type: application/json" -X DELETE  http://$KONG_PROXY:32080/appmgr/ric/v1/xapps/anomaly-python
```
