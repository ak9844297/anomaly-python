# ==================================================================================
#
#       Copyright (c) 2020 Samsung Electronics Co., Ltd. All Rights Reserved.
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#          http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
#
# ==================================================================================
import redis
import time
import json
import sys
from faker import Faker
from .hwxapp import HWXapp

_redis = None
fake=Faker()
logpath = "/opt/anomalyXapp.log"
logwriter=open(logpath,'w')
def appmgrAttack():
        data=[]
        appmgrkey="{appmgr},"+fake.bothify(text='#??#??#???????#??#?#??????#')
        appmgrSchema = {
                "data": {
                        "eventType": fake.bothify(text='????'),
                        "maxRetries": fake.random_digit(),
                        "retryTimer": fake.random_digit(),
                        "targetUrl": fake.url()
                }
        }
        data.append(appmgrkey)
        data.append(appmgrSchema)
        _redis.set(appmgrkey,json.dumps(appmgrSchema))
        return data
def e2TaddrAttack():
        data=[]
        e2TaddrKey="{e2Manager},E2TAddresses"
        e2Taddr=f"{fake.ipv4_private()}:{fake.port_number()}"
        e2TaddrSchema=[e2Taddr]
        _redis.set(e2TaddrKey,json.dumps(e2TaddrSchema))
        data.append(e2TaddrKey)
        data.append(e2TaddrSchema)
        return data
def submgraddrAttack():
        data=[]
        submgrInstanceid=fake.random_int(min=0, max=1000)
        submgrid=fake.random_number() #for subreq resp
        submgrKey="{submgr_e2SubsDb},"+f"{submgrid}" #未驗證
        submgrSchema={  
        "Valid": fake.boolean(),
                "ReqId": {
                        "Id": fake.random_number(), 
                        "InstanceId": submgrInstanceid #長度未知
                },
                "Meid": {
                        "PlmnId": fake.pystr(), #長度未知
                        "EnbID": fake.pystr(), #長度未知
                        "RanName": fake.pystr() #長度未知
                },
                "EpList": {
                        "Endpoints": [
                                {"Addr": fake.ipv4_private(),
                                "Port": fake.port_number()}
                        ]
                },
                "SubReqMsg": {
                        "Id": submgrid, #長度未知
                        "InstanceId": submgrInstanceid, #長度未知
                        "FunctionId": fake.random_number(), #長度未知
                        "Data": {
                                "Length": fake.random_int(min=10, max=20),#長度未知
                                "Data": fake.pystr() #長度未知
                        },
                        "ActionSetups": 
                        [
                                {"ActionId":fake.random_number(), #長度未知
                                "ActionType": fake.random_number(), #長度未知
                                "RicActionDefinitionPresent":fake.boolean(),
                                "Data": {
                                        "Length": fake.random_int(min=10, max=20),#長度未知
                                        "Data": None
                                },
                                "Present": fake.boolean(),
                                "Type": fake.random_digit(),#長度未知
                                "TimetoWait": fake.random_number()
                                } #長度未知                 
                        ]
                        
                },
                "SubRespMsg": {
                        "Id": submgrid, #長度未知
                        "InstanceId": submgrInstanceid, #長度未知
                        "FunctionId": fake.random_number(),
                        "SubRespRcvd": fake.pystr(), #長度未知
                        "PolicyUpdate": fake.boolean(),#長度未知
                        "ActionAdmittedList": {
                                "Items": 
                                [
                                        {"ActionId": fake.random_number()} #長度未知
                                ]               
                        },
                        "ActionNotAdmittedList": {
                                "Items": []
                        }
                } 
        }
        _redis.set(submgrKey,json.dumps(submgrSchema))
        data.append(submgrKey)
        data.append(submgrSchema)
        return data
def e2generalAttack():
        data=[]
        e2generalKey="{e2Manager},GENERAL" 
        e2generalSchema = {
                "enableRic": fake.boolean()
        }
        _redis.set(e2generalKey,json.dumps(e2generalSchema))
        data.append(e2generalKey)
        data.append(e2generalSchema)
        return data
def e2eNBAttack():
        data=[]
        e2eNBKey="{e2Manager},ENB"
        e2eNBSchema = fake.pystr()
        data.append(e2eNBKey)
        data.append(e2eNBSchema)
        _redis.sadd(e2eNBKey,e2eNBSchema)
        return data
#{e2Manager},RAN:enB_macro_001_001_0019b0
def e2RanAttack():
        data=[]
        e2RanKey="{e2Manager},RAN:"+fake.bothify(text='enB_macro_###_###_####?#')
        e2RanShema = fake.pystr()
        _redis.set(e2RanKey,e2RanShema)
        data.append(e2RanKey)
        data.append(e2RanShema)
        return data
#{e2Manager},ENB:00F110:00000000000110011011
def e2eNBAddrAttack():
        data=[]
        e2eNBAddrKey="{e2Manager},ENB:"+fake.bothify(text='##?###:####################')#未驗證
        e2eNBAddrSchema = fake.pystr()
        _redis.set(e2eNBAddrKey,e2eNBAddrSchema)
        data.append(e2eNBAddrKey)
        data.append(e2eNBAddrSchema)
        return data
def e2InstanceAttack():
        data=[]
        e2Taddr=f"{fake.ipv4_private()}:{fake.port_number()}"
        e2InstanceKey="{e2Manager},E2TInstance:"+e2Taddr
        e2InstanceSchema = { #未驗證
                "address": fake.ipv4_private(),
                "podName": "e2term",
                "associatedRanList": fake.pylist(value_types='str'),
                "keepAliveTimestamp": fake.random_number(), #長度未知
                "state": fake.pystr(), #長度未知
                "deletionTimeStamp": fake.random_number() #長度未知      
        }
        _redis.set(e2InstanceKey,json.dumps(e2InstanceSchema))
        data.append(e2InstanceKey)
        data.append(e2InstanceSchema)
        return data
def ueJsonAttack():
        data=[]
        ueJsonkey=fake.random_int(min=0, max=1000)
        ueJsonSchema = {
                "PDCP-Bytes-DL": fake.random_number(), #長度未知
                "Neighbor-Cell-RF": None,
                "UE ID": fake.pystr(min_chars= 1, max_chars = 20),
                "Meas-Time-RF": {
                        "tv_sec": fake.random_number(), #長度未知
                        "tv_nsec": fake.random_number(), #長度未知
                },
                "Meas-Timestamp-PDCP-Bytes": {
                        "tv_sec": fake.random_number(), #長度未知
                        "tv_nsec": fake.random_number() #長度未知
                },
                "PRB-Usage-UL": fake.random_number(), #長度未知
                "Serving-Cell-RF": {
                        "rsrq": fake.random_number(), #長度未知
                        "rsrp": fake.random_number(), #長度未知
                        "rsSinr": fake.random_number(), #長度未知
                },
                "PDCP-Bytes-UL": fake.random_number(), #長度未知
                "Serving Cell ID": fake.pystr(min_chars= 1, max_chars = 20),
                "PRB-Usage-DL": fake.random_number(), #長度未知
                "Meas-Timestamp-PRB": {
                        "tv_sec": fake.random_number(), #長度未知
                        "tv_nsec": fake.random_number() #長度未知
                }
        }
        _redis.set(ueJsonkey,json.dumps(ueJsonSchema))
        data.append(ueJsonkey)
        data.append(ueJsonSchema)
        return data
def rmfake(dataset):
        logwriter.write(f"TS : {time.time()} Fake data removed key :{dataset[0]}\n")
        _redis.delete(dataset[0])
def launchXapp():
    hwxapp = HWXapp()
    hwxapp.start()
    try:
                _redis=redis.Redis(host="10.244.0.14", port=6379,
                     db=0, decode_responses=True)
    except :
                logwriter.write(f"can't connect to redisDB!!")
    while True:
                cmd=fake.random_int(min=0, max=8)
                data=[]
                # 3.10 有match case 但Dokcer是3.8所以用if else
                if (cmd==0):
                        logwriter.write(f"TS : {time.time()} Start appmgrAttack ... fake data will remove in 2 sec\n")
                        data=appmgrAttack()
                elif (cmd==1):
                        logwriter.write(f"TS : {time.time()} Start e2TaddrAttack ... fake data will remove in 2 sec\n")
                        data=e2TaddrAttack()
                        time.sleep(2)
                        logwriter.write(f"TS : {time.time()} Fake data removed key :{data[0]}\n")
                        reale2Tkey="{e2Manager},E2TAddresses"
                        reale2T=["10.96.155.134:38000"]
                        _redis.set(reale2Tkey,json.dumps(reale2T))
                        continue
                elif (cmd==2):
                        logwriter.write(f"TS : {time.time()} Start submgraddrAttack ... fake data will remove in 2 sec\n")
                        data=submgraddrAttack()
                elif (cmd==3):
                        logwriter.write(f"TS : {time.time()} Start e2generalAttack ... fake data will remove in 2 sec\n")
                        data=e2generalAttack()
                        e2gKey="{e2Manager},GENERAL" 
                        e2gSchema = {
                        "enableRic": True
                                }
                        logwriter.write(f"TS : {time.time()} Fake data removed key :{data[0]}\n")
                        _redis.set(e2gKey,json.dumps(e2gSchema))
                        continue
                elif (cmd==4):
                        logwriter.write(f"TS : {time.time()} Start e2eNBAttack ... fake data will remove in 2 sec\n")
                        data=e2eNBAttack()
                        time.sleep(2)
                        logwriter.write(f"TS : {time.time()} Fake data removed key :{data[0]}\n")
                        _redis.srem(data[0],data[1])
                        continue
                elif (cmd==5):
                        logwriter.write(f"TS : {time.time()} Start e2RanAttack ... fake data will remove in 2 sec\n")
                        data=e2RanAttack()
                elif (cmd==6):
                        logwriter.write(f"TS : {time.time()} Start e2eNBAddrAttack ... fake data will remove in 2 sec\n")
                        data=e2eNBAddrAttack()
                elif (cmd==7):
                        logwriter.write(f"TS : {time.time()} Start e2InstanceAttack... fake data will remove in 2 sec\n")
                        data=e2InstanceAttack()
                else :
                        logwriter.write(f"TS : {time.time()} Start ueJsonAttack ... fake data will remove in 2 sec\n")
                        data=ueJsonAttack()
                time.sleep(2)
                rmfake(data)


if __name__ == "__main__":
    launchXapp()
